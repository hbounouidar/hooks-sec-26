import {useReducer, useCallback} from 'react';

const initialeState ={
  loading : false, 
  erro : null,
  data : null,
  extra : null,
  identifier : null
};

const httpReducer = (curhttpState, action) => {
    switch (action.type) {
      case 'SEND' : 
        return {loading : true, error : null, data :null, extra : null, identifier :action.identifier};
      case 'RESPONSE' : 
        return {...curhttpState, loading : false, data:action.responseData, extra : action.extra};
      case 'ERROR' : 
        return {loading : false, error : action.errorMessage};
      case 'CLEAR' : 
        return initialeState
      default :
        throw new Error('sould not get there !');
    }
};

const useHttp = () => {
    const [httpState,dispatchHttp ] = useReducer(httpReducer, initialeState);

    const clear = useCallback(() => dispatchHttp({type : 'CLEAR'}), [])

      const sendRequest =useCallback((url, method, body, reqExtra, reqIdentifier) => {
        dispatchHttp({type : 'SEND', identifier :reqIdentifier });
        fetch(url,
        {
         method: method,
         body : body,
         headers : {
             'Content-type' : 'application/json'
         }
       })
       .then(response => { return response.json(); })
       .then (responseData => {
           dispatchHttp({type : 'RESPONSE', responseData :responseData, extra :reqExtra});

       }).catch (err => {
         //setError("Somthing wrong : " +err.message)
         dispatchHttp({type : 'ERROR', errorMessage : 'somthing wrong !'});
       });
      },[]);

   return {
     isLoading : httpState.loading,
     data:httpState.data,
     error:httpState.error,
     sendRequest : sendRequest,
     reqExtra : httpState.extra,
     reqIdentifier : httpState.identifier,
     clear : clear
   };   
      
};

export default useHttp;